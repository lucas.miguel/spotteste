$(document).ready(function(){
    //botao do menu
    $('.abrir').click(function(){
        $(this).addClass('d-none');
        //adiciono a classe block no X
        $('.fechar').addClass('d-block');
        $('.menu').addClass('open');
        $('body').addClass('overflow-hidden');
    });
    $('.fechar').click(function(){
        $(this).removeClass('d-block');//aqui para fechar o menu
        $('.menu').removeClass('d-block open').show('slow');//aqui para fechar o menu
        $('.abrir').removeClass('d-none');//para aparecer o menu hamburguer
        $('body').removeClass('overflow-hidden');
    });
        
});
//carregando function para cadastrar ou editar categoria
function tipoCadastroCategoria(tipo,dados){
    var dadosForm = dados;
    $.ajax({
        type:"POST",
        url:"./querys/insertCategoria.php",
        data:dadosForm,
        success:function(result){
            console.log(result);
            var categorias = JSON.parse(result);
            if(tipo == 2){
                $("#categoriaModal").modal("hide");
                console.log(categorias);
                var categoriaNome = $("#categoriaNome").val();
                var descricaoCategoria = $("#descricaoCategoria").val();
                var cod_categoria = $("#cod_categoria").val();
                var linhas = $("#tbCategorias>tbody>tr");
                var e = linhas.filter(function(i,elemento){
                    return elemento.cells[0].textContent == cod_categoria;
                });
                if(e){
                    e[0].cells[0].textContent = cod_categoria;
                    e[0].cells[1].textContent = categoriaNome;
                    e[0].cells[2].textContent = descricaoCategoria;
                    $("#msgModal ").modal("show");
                    var msgAtualizado = "Atualizado com sucesso!";
                    $("#msgModal .modal-content").html("<p class='text-center'>" + msgAtualizado + "</p>" );
                    
                }              
            }else{
                $("#categoriaModal").modal("hide");
                $("#msgModal ").modal("show");
                var msgAtualizado = "Cadastrado com sucesso!";
                $("#msgModal .modal-content").html("<p class='text-center'>" + msgAtualizado + "</p>" );
                location.reload();
            };
        },
        error:function(){
            console.log("deu ruim");
        }
    });
}
//carregando modal para cadastro de categorias
function categoriaModal(){
    $("#categoriaNome").val('');
    $("#descricaoCategoria").val('');
    $("#cod_categoria").val('');
    $("#categoriaModal").modal("show");
    $("#tipoCadastro").val(1);
}
//adicionando eventos para submite
$("#categoriasForm").submit(function(event){
    event.preventDefault();
    
    var form = $(this).serialize();
    var tipoCadastro = $("#tipoCadastro").val();
    
    tipoCadastroCategoria(tipoCadastro,form);
    
});
//funcao para editar categoria via ajax
function editarModal(id){
    var idCategoria = id;
    
    $("#categoriaModal").modal("show");
    $.ajax({
        type:"GET",
        url:"./querys/getJson.php?edit=" + idCategoria,
        success:function(result){
            var categoria = JSON.parse(result);
            $("#categoriaNome").val(categoria.nome);
            $("#descricaoCategoria").val(categoria.descricao);
            $("#tipoCadastro").val(2);
            $("#cod_categoria").val(idCategoria);
                         
        },
        error:function(){
            console.log("deu ruim");
        }
    });
}
//funcao para delete via ajax categoria
function deleteCategoria(id){
    var idCategoria = id;
    $.ajax({
        type:"GET",
        url:"./querys/deletCategoria.php?delete=" + idCategoria,
        success:function(result){
            var categoria = JSON.parse(result);
            console.log(categoria);
            var linhas = $("#tbCategorias>tbody>tr");
            var e = linhas.filter(function(i,elemento){
                return elemento.cells[0].textContent == id;
            });
            if(e){
                e.remove();
                $("#msgModal").modal("show");
                var msgDelete = "Deletado com sucesso!";
                $("#msgModal .modal-content").html("<p class='text-center'>" + msgDelete + "</p>" );
                
            }  
              
            
        },
        error:function(){
            console.log("deu ruim");
        }
    });
}
//Carregando funcoes dos produtos
function tipoCadastroProdutos(tipo,dados){
    var dadosForm = dados;
    $.ajax({
        type:"POST",
        url:"./querys/produtosInsert.php",
        data:dadosForm,
        success:function(result){
            var produtos = JSON.parse(result);
            console.log(produtos);
            if(tipo == 2){
                $("#produtosModal").modal("hide");
                var nomeProduto = $("#nomeProduto").val();
                var cod_produto = $("#cod_produtos").val();
                var nomeCategoria = $("#categoria option:selected").text();
                var linhas = $("#tbProdutos>tbody>tr");
                var e = linhas.filter(function(i,elemento){
                    return elemento.cells[0].textContent == cod_produto;
                });
                if(e){
                    e[0].cells[0].textContent = cod_produto;
                    e[0].cells[1].textContent = nomeProduto;
                    e[0].cells[2].textContent = produtos.detalhe;
                    e[0].cells[3].textContent = nomeCategoria;
                    e[0].cells[4].textContent = produtos.preco;
                    $("#msgModal ").modal("show");
                    var msgAtualizado = "Produto editado com sucesso!";
                    $("#msgModal .modal-content").html("<p class='text-center'>" + msgAtualizado + "</p>" );
                    
                }              
            }else{
                $("#categoriaModal").modal("hide");
                $("#msgModal ").modal("show");
                var msgAtualizado = "Cadastrado com sucesso!";
                $("#msgModal .modal-content").html("<p class='text-center'>" + msgAtualizado + "</p>" );
                location.reload();
            };
        },
        error:function(){
            console.log("deu ruim");
        }
    });
}

function modalProduto(){
    $("#tipoCadastroProdutos").val(1);
    $("#nomeProduto").val('');
    $("#preco").val('');
    $("#detalhe").val('');
    $("#produtosModal").modal('show');
    
}
//submit produtos
$("#produtosCrud").submit(function(event){
    event.preventDefault();
    
    var form = $(this).serialize();
    var tipoCadastro = $("#tipoCadastroProdutos").val();
    
    tipoCadastroProdutos(tipoCadastro,form);
    
});
function editarProduto(id){
    var idProdutos = id;
    $("#produtosModal").modal("show");
    $.ajax({
        type:"GET",
        url:"./querys/getJson.php?editProduto=" + idProdutos,
        success:function(result){
            var produtos = JSON.parse(result);
            $("#tipoCadastroProdutos").val(2);
            $("#nomeProduto").val(produtos.nomeProduto);
            $("#preco").val(produtos.valor);
            $("#detalhe").val(produtos.descricaoProduto);
            $("#cod_produtos").val(produtos.cod_produto);                        
        },
        error:function(){
            console.log("deu ruim");
        }
    });
}
//funcao para delete produtos via ajax 
function deleteProduto(id){
    var idProduto= id;
    $.ajax({
        type:"GET",
        url:"./querys/deleteProduto.php?deleteProduto=" + idProduto,
        success:function(result){
            var Produtos = JSON.parse(result);
            console.log(Produtos);
            var linhas = $("#tbProdutos>tbody>tr");
            var e = linhas.filter(function(i,elemento){
                return elemento.cells[0].textContent == id;
            });
            if(e){
                e.remove();
                $("#msgModal").modal("show");
                var msgDelete = "Produto Deletado com sucesso!";
                $("#msgModal .modal-content").html("<p class='text-center'>" + msgDelete + "</p>" );
                
            }  
              
            
        },
        error:function(){
            console.log("deu ruim");
        }
    });
}
