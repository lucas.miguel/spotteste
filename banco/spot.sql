-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 25-Ago-2020 às 15:00
-- Versão do servidor: 10.4.13-MariaDB
-- versão do PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `spot`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ativo`
--

CREATE TABLE `ativo` (
  `cod_ativo` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `cod_categoria` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `ativo_cod` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`cod_categoria`, `nome`, `descricao`, `ativo_cod`, `created`, `modified`) VALUES
(1, 'celular teste', 'sdasfsdfsdfsd  dsdsdad', 2, '2020-08-25 01:55:07', '2020-08-25 04:46:22'),
(2, 'celular', '', 2, '2020-08-25 01:55:31', '2020-08-25 04:29:48'),
(3, 'celular', '', 2, '2020-08-25 01:59:54', '2020-08-25 04:31:16'),
(4, 'geladeira', 'brastemp', 2, '2020-08-25 02:48:26', '2020-08-25 04:34:17'),
(5, 'geladeira', 'eletrolux', 2, '2020-08-25 02:49:35', '2020-08-25 04:34:19'),
(6, 'geladeira', 'eletrolux', 2, '2020-08-25 02:49:40', '2020-08-25 04:35:45'),
(7, 'geladeira', 'eletrolux', 2, '2020-08-25 02:53:24', '2020-08-25 04:35:56'),
(8, 'geladeira', 'eletrolux', 2, '2020-08-25 02:54:59', '2020-08-25 04:35:57'),
(9, 'geladeira', 'brastemp', 2, '2020-08-25 03:00:14', '2020-08-25 04:35:57'),
(10, 'geladeira', 'brastemp', 2, '2020-08-25 03:01:05', '2020-08-25 04:35:58'),
(11, 'geladeira', 'brastemp', 2, '2020-08-25 03:01:10', '2020-08-25 04:37:30'),
(12, 'geladeira', 'brastemp', 2, '2020-08-25 03:01:28', '2020-08-25 04:37:56'),
(13, 'celular teste', '', 2, '2020-08-25 01:55:07', '2020-08-25 04:38:55'),
(14, 'geladeira', 'brastemp', 2, '2020-08-25 03:07:55', '2020-08-25 04:41:42'),
(15, 'geladeira', 'brastemp', 2, '2020-08-25 03:23:29', '2020-08-25 04:42:38'),
(16, 'geladeira', 'brastemp', 2, '2020-08-25 03:24:55', '2020-08-25 04:43:44'),
(17, 'geladeira', 'brastemp', 2, '2020-08-25 03:26:23', '2020-08-25 04:45:49'),
(18, 'geladeira', 'brastemp', 2, '2020-08-25 03:26:54', '2020-08-25 04:46:26'),
(19, 'geladeira ', 'brastemp ', 2, '2020-08-25 03:27:15', '2020-08-25 05:44:08'),
(20, 'geladeira', 'brastemp', 2, '2020-08-25 03:29:32', '2020-08-25 05:44:11'),
(21, 'geladeira', 'brastemp ', 1, '2020-08-25 03:29:52', '2020-08-25 06:10:30'),
(22, 'geladeira', 'brastemp ', 1, '2020-08-25 04:46:40', '2020-08-25 06:06:47'),
(23, 'geladeira uuuuuuu', 'brastemp', 1, '2020-08-25 05:46:21', '2020-08-25 05:46:21'),
(24, 'geladeira ', ' df df df dfdf f', 1, '2020-08-25 05:47:02', '2020-08-25 06:08:45'),
(25, 'geladeira uuuuuuu', 'lenovo', 1, '2020-08-25 06:04:15', '2020-08-25 06:17:58'),
(26, 'racão', 'brastemp dsds', 1, '2020-08-25 06:10:43', '2020-08-25 09:03:44'),
(27, 'geladeira ', 'brastemp ', 2, '2020-08-25 06:12:44', '2020-08-25 06:14:44'),
(28, 'celular', 'brastemp dsds', 2, '2020-08-25 06:14:18', '2020-08-25 06:14:40'),
(29, 'celular', 'lenovo', 2, '2020-08-25 06:17:48', '2020-08-25 06:17:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `cod_produto` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `valor` varchar(200) DEFAULT NULL,
  `ativo_cod` int(11) NOT NULL,
  `cod_categoria` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`cod_produto`, `nome`, `descricao`, `valor`, `ativo_cod`, `cod_categoria`, `created`, `modified`) VALUES
(1, 'etetet teste', 'erwer', '4334', 2, 21, '2020-08-25 08:12:02', '2020-08-25 09:51:58'),
(2, 'wrerererer', 'erwersdcsdfsdf', '4334', 2, 23, '2020-08-25 09:53:01', '2020-08-25 09:53:07'),
(3, 'geladeira', 'brastemp ', '4334', 2, 24, '2020-08-25 09:53:35', '2020-08-25 09:53:41'),
(4, 'wrerererer', 'erwersdcsdfsdf', '4334', 1, 23, '2020-08-25 09:58:51', '2020-08-25 09:58:51');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `ativo`
--
ALTER TABLE `ativo`
  ADD PRIMARY KEY (`cod_ativo`);

--
-- Índices para tabela `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`cod_produto`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `ativo`
--
ALTER TABLE `ativo`
  MODIFY `cod_ativo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `categoria`
--
ALTER TABLE `categoria`
  MODIFY `cod_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `cod_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
