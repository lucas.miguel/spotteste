<?php
    
    include 'banco/conexao.php';
    include 'header/index.php';
    global $tituloPagina;
    $tituloPagina = "Categorias";
    include 'header/menu.php';
    include 'querys/getCategorias.php';
    
?>

<div class="container">
    <div class="row">
        
        <?php if($categoria == 0): ?>
            <div id="formCategoria" class="col-12  col-md-8 mleft-auto mleright-auto border">
                <form method="POST">
                    <div class="form-group">
                        <label for="nomeCategoria">Nome</label>
                        <input type="text" class="form-control" name="nomeCategoria" id="nomeCategoria">
                        <small  class="form-text text-muted">Cadastrar nome da categoria.</small>
                    </div>
                    <div class="form-group">
                        <label for="descricaoCategoria">Categoria</label>
                        <input type="text" class="form-control" nome="descricaoCategoria" id="descricaoCategoria">
                        <small  class="form-text text-muted">Descricao da categoria.</small>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </form>
                
            </div>
                    
        <?php else : ?>
            <div class="col-12 categorias table-responsive border">
                <h5 class="card-title">Categorias cadastradas</h5>
                <table id="tbCategorias" class="table table-ordered table-hover ">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>categoria</th>
                            <th>Descrição</th>
                            <th>criado</th>
                            <th class="text-center">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while($resultado = $resultsCategorias->fetch_object()):?>
                            <tr>
                                <td><?php echo $resultado->cod_categoria ?></td>
                                <td><?php echo $resultado->nome ?></td>
                                <td><?php echo $resultado->descricao ?></td>
                                <td><?php echo $resultado->created ?></td>
                                <td class="d-flex justify-content-around align-items-center">
                                    <a href="javascript:void(0)" role="button" class="btn btn-outline-primary btnEditar" onClick="editarModal(<?php echo $resultado->cod_categoria ?>)">Editar</a>
                                    <a href="javascript:void(0)" class="btn btn-outline-danger" onClick="deleteCategoria(<?php echo $resultado->cod_categoria ?>)">Excluir</a>
                                </td>
                            </tr>

                        <?php endwhile ?>
                    </tbody>
                </table>
                <a href="javascript:void(0)" role="button" onClick="categoriaModal()" class="btn btn-outline-primary" >Nova categoria</a>
            </div>   
        <?php endif ?>     
    </div>
</div>

<?php
    include 'modal/cadastroCategoria.php';
    include 'modal/modalMsg.php';
    include 'footer/index.php';
?>