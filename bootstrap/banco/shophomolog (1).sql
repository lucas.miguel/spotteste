-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 24-Jan-2020 às 21:36
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `shophomolog`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acessorios`
--

CREATE TABLE `acessorios` (
  `id` int(11) NOT NULL,
  `tipo_videos_id` int(11) NOT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `sobre` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `acessorios`
--

INSERT INTO `acessorios` (`id`, `tipo_videos_id`, `nome`, `titulo`, `sobre`, `imagem`, `descricao`, `valor`, `created`, `modified`) VALUES
(1, 1, 'locução feminina', 'ESCOLHA O ESTILO DA LOCUÇÃO', 'Dê voz e um toque mais humano aos seus vídeos! Contrate o serviço de locução e divulgue seus produtos e serviços utilizando a opção de voz feminina ou masculina.', './img/icones/feminino.png', 'locução para videos de 15 seg', 99, '2020-01-07 12:00:40', '2020-01-07 12:00:40'),
(2, 2, 'locução feminina', '', '', '', 'locução para videos de 30 seg', 149, '2020-01-07 11:15:47', '2020-01-07 11:15:47'),
(3, 3, 'locução feminina', '', '', '', 'locução para videos de 60 seg', 199, '2020-01-07 11:15:47', '2020-01-07 11:15:47'),
(4, 1, 'locução masculina ', '', '', './img/icones/masculino.png', 'locução para videos de 15 seg', 99, '2020-01-07 11:28:58', '2020-01-07 11:28:58'),
(5, 2, 'locução masculina ', '', '', '', 'locução para videos de 30 seg', 149, '2020-01-07 11:28:59', '2020-01-07 11:28:59'),
(6, 3, 'locução masculina ', '', '', '', 'locução para videos de 60 seg', 199, '2020-01-07 11:28:59', '2020-01-07 11:28:59'),
(7, 1, 'vetorização da logomarca ', '', '', '', 'vetorizção de logo ', 149, '2020-01-07 11:28:59', '2020-01-07 11:28:59'),
(8, 1, 'roteiro ', 'ROTEIRO (TEXTO DO VÍDEO)', 'Torne a comunicação do seu vídeo ainda mais assertiva e impactante com o serviço de desenvolvimento de roteiro.', '', 'texto para vídeo 15 seg', 69, '2020-01-07 11:28:59', '2020-01-07 11:28:59'),
(9, 2, 'roteiro ', '', '', '', 'texto para vídeo 30 seg', 129, '2020-01-07 11:28:59', '2020-01-07 11:28:59'),
(10, 3, 'roteiro ', '', '', '', 'texto para vídeo 60 seg', 149, '2020-01-07 11:28:59', '2020-01-07 11:28:59'),
(11, 1, 'tratamento de imagens ', 'TRATAMENTO DE IMAGENS', 'Aprimore suas fotos e vídeos, garantindo a qualidade que seu vídeo precisa para alavancar suas vendas!', '', 'imagens para vídeos de 15 seg', 79, '2020-01-07 11:30:31', '2020-01-07 11:30:31'),
(12, 2, 'tratamento de imagens ', '', '', '', 'imagens para vídeos de 30 seg', 79, '2020-01-07 11:30:31', '2020-01-07 11:30:31'),
(13, 3, 'tratamento de imagens ', '', '', '', 'imagens para vídeos de 15 seg', 79, '2020-01-07 11:30:31', '2020-01-07 11:30:31'),
(14, 1, 'fotos para videos', 'FOTOS', 'Seus vídeos com fotos profissionais e únicas! Contrate o serviço de fotógrafo do SHOP Empreendedor.', '', 'fotos para vídeos de 15 seg', 79, '2020-01-07 11:34:12', '2020-01-07 11:34:12'),
(15, 2, 'fotos para videos', '', '', '', 'fotos para vídeos de 30 seg', 80, '2020-01-07 11:34:12', '2020-01-07 11:34:12'),
(16, 3, 'fotos para videos', '', '', '', 'fotos para vídeos de 60 seg', 79, '2020-01-07 11:34:12', '2020-01-07 11:34:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `carrinho`
--

CREATE TABLE `carrinho` (
  `id` int(11) NOT NULL,
  `session` varchar(64) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `carrinho`
--

INSERT INTO `carrinho` (`id`, `session`, `id_usuario`, `valor`, `created`) VALUES
(0, NULL, 92, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `observacao` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `status_id`, `nome`, `observacao`, `created`, `modified`) VALUES
(1, 1, 'beleza', NULL, NULL, NULL),
(2, 1, 'consultoria', NULL, NULL, NULL),
(3, 1, 'esporte', NULL, NULL, NULL),
(4, 1, 'gastronomia', NULL, NULL, NULL),
(5, 1, 'manutencao', NULL, NULL, NULL),
(6, 1, 'moda', NULL, NULL, NULL),
(7, 1, 'saude', NULL, NULL, NULL),
(8, 1, 'diversos', 'outros videos', '2019-12-11 21:52:00', '2019-12-11 21:52:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nome`
--

CREATE TABLE `nome` (
  `name` varchar(52) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `categorias_id` int(11) NOT NULL,
  `tipo_videos_id` int(11) NOT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `imagem` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `linkvideo` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `status_id`, `categorias_id`, `tipo_videos_id`, `nome`, `preco`, `descricao`, `imagem`, `linkvideo`, `created`, `modified`) VALUES
(21, 1, 1, 2, 'beleza', 899, 'MODELO 1 30SEG', './img/categorias/beleza/MODELO-01-30-SEG.jpg', 'https://youtu.be/SLpgCCeoZKc', '2019-12-11 16:49:36', '2019-12-11 16:49:36'),
(22, 1, 1, 2, 'beleza', 1299, 'MODELO 1 60SEG', './img/categorias/beleza/MODELO-01-60-SEG.jpg', 'https://youtu.be/2s_QFcCxoPY', '2019-12-11 16:52:29', '2019-12-11 16:52:29'),
(92, 1, 1, 1, 'beleza', 359, 'MODELO 1 15SEG', './img/categorias/beleza/MODELO-01-15-SEG.jpg', 'https://youtu.be/bGd2UHqb_Xg', '2019-11-25 17:34:24', '2019-11-25 17:34:24'),
(93, 1, 1, 2, 'beleza', 899, 'MODELO 2 30SEG', './img/categorias/beleza/MODELO-02-30-SEG.jpg', 'https://youtu.be/tyZu3P859Sk', '2019-12-11 16:49:36', '2019-12-11 16:49:36'),
(94, 1, 1, 2, 'beleza', 1299, 'MODELO 2 60SEG', './img/categorias/beleza/MODELO-02-60-SEG.jpg', 'https://youtu.be/Y_6BatCf7f0', '2019-12-11 16:57:03', '2019-12-11 16:57:03'),
(95, 1, 1, 2, 'beleza', 899, 'MODELO 3 30SEG', './img/categorias/beleza/MODELO-03-30-SEG.jpg', 'https://youtu.be/KzQRcXtipcI', '2019-12-11 16:58:32', '2019-12-11 16:58:32'),
(96, 1, 1, 2, 'beleza', 1299, 'MODELO 3 60SEG', './img/categorias/beleza/MODELO-03-60-SEG.jpg', 'https://youtu.be/JIcJOA6zSZc', '2019-12-11 16:57:03', '2019-12-11 16:57:03'),
(97, 1, 1, 1, 'beleza', 359, 'MODELO 2 15SEG', './img/categorias/beleza/MODELO-02-15-SEG.jpg', 'https://youtu.be/ioNdEEueEjk', '2019-12-11 17:06:56', '2019-12-11 17:06:56'),
(98, 1, 1, 2, 'beleza', 899, 'MODELO 4 30SEG', './img/categorias/beleza/MODELO-04-30-SEG.jpg', 'https://youtu.be/cd4fBCXfAiM', '2019-12-11 17:07:41', '2019-12-11 17:07:41'),
(99, 1, 1, 2, 'beleza', 1299, 'MODELO 4 60SEG', './img/categorias/beleza/MODELO-04-60-SEG.jpg', 'https://youtu.be/_fxPJXUtfWs', '2019-12-11 17:02:56', '2019-12-11 17:02:56'),
(100, 1, 1, 1, 'beleza', 359, 'MODELO 3 15SEG', './img/categorias/beleza/MODELO-03-15-SEG.jpg', 'https://youtu.be/NLz1YE0Gu2k', '2019-12-11 16:57:51', '2019-12-11 16:57:51'),
(101, 1, 1, 2, 'beleza', 899, 'MODELO 5 30SEG', './img/categorias/beleza/MODELO-05-30-SEG.jpg', 'https://youtu.be/ffHuQAb2_3g', '2019-12-11 17:04:40', '2019-12-11 17:04:40'),
(102, 1, 1, 1, 'beleza', 359, 'MODELO 4 15SEG', './img/categorias/beleza/MODELO-04-15-SEG.jpg', 'https://youtu.be/1_lqAktAMUw', '2019-12-11 16:57:51', '2019-12-11 16:57:51'),
(103, 1, 1, 2, 'beleza', 1299, 'MODELO 5 60SEG', './img/categorias/beleza/MODELO-05-60-SEG.jpg', 'https://youtu.be/0Pl2srjEZUI', '2019-12-11 17:05:21', '2019-12-11 17:05:21'),
(500, 1, 1, 1, 'beleza', 359, 'MODELO 5 15SEG', './img/categorias/beleza/MODELO-05-15-SEG.jpg', 'https://youtu.be/xXn_Le8f91U', '2019-12-11 17:03:59', '2019-12-11 17:03:59'),
(1012, 1, 2, 1, 'consultoria', 359, 'MODELO 01 15SEG', './img/categorias/consultoria/MODELO-01-15-SEG.jpg', 'https://youtu.be/dHFrr6ostoE ', '2019-12-11 15:57:55', '2019-12-11 15:57:55'),
(1013, 1, 2, 2, 'consultoria', 899, 'MODELO 01 30SEG', './img/categorias/consultoria/MODELO-01-30-SEG.jpg', 'https://youtu.be/V4a2uLD1rCo', '2019-12-11 16:16:11', '2019-12-11 16:16:11'),
(1014, 1, 2, 2, 'consultoria', 899, 'MODELO 02 30SEG', './img/categorias/consultoria/MODELO-02-30-SEG.jpg', 'https://youtu.be/vqQylku5cOc', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1015, 1, 2, 1, 'consultoria', 359, 'MODELO 03 15SEG', './img/categorias/consultoria/MODELO-03-15-SEG.jpg', 'https://youtu.be/rXWDGcbyvPI', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1016, 1, 2, 2, 'consultoria', 899, 'MODELO 03 30SEG', './img/categorias/consultoria/MODELO-03-30-SEG.jpg', 'https://youtu.be/8MNWiMClAQM', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1017, 1, 2, 1, 'consultoria', 359, 'MODELO 04 15SEG', './img/categorias/consultoria/MODELO-04-15-SEG.jpg', 'https://youtu.be/8tQ-XqEi7rI', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1018, 1, 2, 2, 'consultoria', 899, 'MODELO 04 30SEG', './img/categorias/consultoria/MODELO-04-30-SEG.jpg', 'https://youtu.be/4pZK0s85krE', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1019, 1, 2, 1, 'consultoria', 359, 'MODELO 05 15SEG', './img/categorias/consultoria/MODELO-05-15-SEG.jpg', 'https://youtu.be/ud2BK6DP7M0', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1020, 1, 2, 2, 'consultoria', 899, 'MODELO 05 30SEG', './img/categorias/consultoria/MODELO-05-30-SEG.jpg', 'https://youtu.be/Nj1gI6pqBi8', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1021, 1, 2, 2, 'consultoria', 1299, 'MODELO 05 60SEG', './img/categorias/consultoria/MODELO-05-60-SEG.jpg', 'https://youtu.be/0ubs8iwV5JQ', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1022, 1, 2, 1, 'consultoria', 359, 'MODELO 06 15SEG', './img/categorias/consultoria/MODELO-06-15-SEG.jpg', 'https://youtu.be/6A5bg586gF0', '2019-12-11 22:03:00', '2019-12-11 22:03:00'),
(1023, 1, 2, 2, 'consultoria', 899, 'MODELO 06 30SEG', './img/categorias/consultoria/MODELO-06-30-SEG.jpg', 'https://youtu.be/gKLVnV_lhgM', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1024, 1, 2, 2, 'consultoria', 1299, 'MODELO 06 60SEG', './img/categorias/consultoria/MODELO-06-60-SEG.jpg', 'https://youtu.be/3muJQNi2oEc', '2019-12-11 16:16:12', '2019-12-11 16:16:12'),
(1025, 1, 3, 1, 'esporte', 359, 'MODELO 1 15SEG', './img/categorias/esporte/MODELO-1-15-SEG.jpg', 'https://youtu.be/QZ1jpLMwTt8', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1026, 1, 3, 2, 'esporte', 899, 'MODELO 1 30SEG', './img/categorias/esporte/MODELO-1-30-SEG.jpg', 'https://youtu.be/M1lAWRkHf60', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1027, 1, 3, 1, 'esporte', 359, 'MODELO 2 15SEG', './img/categorias/esporte/MODELO-2-15-SEG.jpg', 'https://youtu.be/-6RajOz_Alo', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1028, 1, 3, 2, 'esporte', 899, 'MODELO 2 30SEG', './img/categorias/esporte/MODELO-2-30-SEG.jpg', 'https://youtu.be/Ews5Kj4oj1Q', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1029, 1, 3, 1, 'esporte', 359, 'MODELO 3 15SEG', './img/categorias/esporte/MODELO-3-15-SEG.jpg', 'https://youtu.be/EjMbXhvak18', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1030, 1, 3, 2, 'esporte', 899, 'MODELO 3 30SEG', './img/categorias/esporte/MODELO-3-30-SEG.jpg', 'https://youtu.be/pYaPCiPgBvs', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1031, 1, 3, 1, 'esporte', 359, 'MODELO 4 15SEG', './img/categorias/esporte/MODELO-4-15-SEG.jpg', 'https://youtu.be/CRpZtX6yZEs', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1032, 1, 3, 2, 'esporte', 899, 'MODELO 4 30SEG', './img/categorias/esporte/MODELO-4-30-SEG.jpg', 'https://youtu.be/FrFfkneNwdw', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1033, 1, 3, 2, 'esporte', 1299, 'MODELO 4 60SEG', './img/categorias/esporte/MODELO-4-60-SEG.jpg', 'https://youtu.be/Rp6cIpyhtAE', '2019-12-11 16:53:28', '2019-12-11 16:53:28'),
(1034, 1, 4, 1, 'gastronomia', 359, 'MODELO 01 15SEG', './img/categorias/gastronomia/MODELO-01-15-SEG.jpg', 'https://youtu.be/RJNvobvXmx0', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1035, 1, 4, 2, 'gastronomia', 899, 'MODELO 01 30SEG', './img/categorias/gastronomia/MODELO-01-30-SEG.jpg', 'https://youtu.be/suY8ELIh_yo', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1036, 1, 4, 1, 'gastronomia', 359, 'MODELO 02 15SEG', './img/categorias/gastronomia/MODELO-02-15-SEG.jpg', 'https://youtu.be/2xRPx9ULxFM', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1037, 1, 4, 2, 'gastronomia', 899, 'MODELO 02 30SEG', './img/categorias/gastronomia/MODELO-02-30-SEG.jpg', 'https://youtu.be/xUQfP2CIa4g', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1038, 1, 4, 1, 'gastronomia', 359, 'MODELO 03 15SEG', './img/categorias/gastronomia/MODELO-03-15-SEG.jpg', 'https://youtu.be/ztN5JZk7o8E', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1039, 1, 4, 2, 'gastronomia', 899, 'MODELO 03 30SEG', './img/categorias/gastronomia/MODELO-03-30-SEG.jpg', 'https://youtu.be/wl87YdSWFg8', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1040, 1, 4, 1, 'gastronomia', 359, 'MODELO 04 15SEG', './img/categorias/gastronomia/MODELO-04-15-SEG.jpg', 'https://youtu.be/prujupHyJbY', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1041, 1, 4, 2, 'gastronomia', 899, 'MODELO 04 30SEG', './img/categorias/gastronomia/MODELO-04-30-SEG.jpg', 'https://youtu.be/uLHMYcNIzhk', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1042, 1, 4, 1, 'gastronomia', 359, 'MODELO 05 15SEG', './img/categorias/gastronomia/MODELO-05-15-SEG.jpg', 'https://youtu.be/qb4Ka_zcS6A', '2019-12-11 17:30:43', '2019-12-11 17:30:43'),
(1043, 1, 4, 2, 'gastronomia', 899, 'MODELO\r\n05 30SEG', './img/categorias/gastronomia/MODELO-05-30-SEG.jpg', 'https://youtu.be/--oYQwH6BIs', '2019-12-11 17:30:44', '2019-12-11 17:30:44'),
(1044, 1, 4, 1, 'gastronomia', 359, 'MODELO 06 15SEG', './img/categorias/gastronomia/MODELO-06-15-SEG.jpg', 'https://youtu.be/0x4otFYIgC8', '2019-12-11 17:30:44', '2019-12-11 17:30:44'),
(1045, 1, 4, 2, 'gastronomia', 899, 'MODELO 06 30SEG', './img/categorias/gastronomia/MODELO-06-30-SEG.jpg', 'https://youtu.be/kbfmJIzdS4A', '2019-12-11 17:30:44', '2019-12-11 17:30:44'),
(1046, 1, 4, 1, 'gastronomia', 359, 'MODELO 07 15SEG', './img/categorias/gastronomia/MODELO-07-15-SEG.jpg', 'https://youtu.be/xhCF5rm_3Og', '2019-12-11 17:30:44', '2019-12-11 17:30:44'),
(1047, 1, 4, 2, 'gastronomia', 899, 'MODELO 07 30SEG', './img/categorias/gastronomia/MODELO-07-30-SEG.jpg', 'https://youtu.be/ZbRO619QYJU', '2019-12-11 17:30:44', '2019-12-11 17:30:44'),
(1048, 1, 5, 1, 'manutenção', 359, 'MODELO 01 15SEG', './img/categorias/manutencao/MODELO-01-15-SEG.jpg', 'https://youtu.be/wF1mLYAsR30', '2019-12-11 17:43:41', '2019-12-11 17:43:41'),
(1049, 1, 5, 2, 'manutenção', 899, 'MODELO 01 30SEG', './img/categorias/manutencao/MODELO-01-30-SEG.jpg', 'https://youtu.be/1pM_QisP5Pc', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1050, 1, 5, 1, 'manutenção', 359, 'MODELO 02 15SEG', './img/categorias/manutencao/MODELO-02-15-SEG.jpg', 'https://youtu.be/hWFjardtD-Q', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1051, 1, 5, 2, 'manutenção', 899, 'MODELO 02 30SEG', './img/categorias/manutencao/MODELO-02-30-SEG.jpg', 'https://youtu.be/1oKfPq1Fqy0', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1052, 1, 5, 1, 'manutenção', 359, 'MODELO 03 15SEG', './img/categorias/manutencao/MODELO-03-15-SEG.jpg', 'https://youtu.be/pivt_3tCtnY', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1053, 1, 5, 2, 'manutenção', 899, 'MODELO 03 30SEG ', './img/categorias/manutencao/MODELO-03-30-SEG.jpg', 'https://youtu.be/Kq9960qMMK0', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1054, 1, 5, 2, 'manutenção', 1299, 'MODELO 03 60SEG', './img/categorias/manutencao/MODELO-03-60-SEG.jpg', 'https://youtu.be/iayaE-fP8n0', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1055, 1, 5, 1, 'manutenção', 359, 'MODELO 04 15SEG', './img/categorias/manutencao/MODELO-04-15-SEG.jpg', 'https://youtu.be/k8uhuRDpza8', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1056, 1, 5, 2, 'manutenção', 1299, 'MODELO 04 60SEG', './img/categorias/manutencao/MODELO-04-60-SEG.jpg', 'https://youtu.be/wmlFib5kNoc ', '2019-12-11 17:43:42', '2019-12-11 17:43:42'),
(1057, 1, 6, 1, 'moda', 359, 'MODELO 1 15SEG', './img/categorias/moda/MODELO-1-15-SEG.jpg', 'https://youtu.be/bOZd1S3aHZE', '2019-12-11 17:54:39', '2019-12-11 17:54:39'),
(1058, 1, 6, 2, 'moda', 899, 'MODELO 1 30SEG', './img/categorias/moda/MODELO-1-30-SEG.jpg', 'https://youtu.be/4pVr_aSHn2E', '2019-12-11 17:54:39', '2019-12-11 17:54:39'),
(1059, 1, 6, 2, 'moda', 899, 'MODELO 2 30SEG', './img/categorias/moda/MODELO-02-30-SEG.jpg', 'https://youtu.be/axbQqIJoWk0', '2019-12-11 17:54:39', '2019-12-11 17:54:39'),
(1060, 1, 6, 1, 'moda', 359, 'MODELO 2 15SEG', './img/categorias/moda/MODELO-2-15-SEG.jpg', 'https://youtu.be/W6dsLKNVc_Y', '2019-12-11 17:54:39', '2019-12-11 17:54:39'),
(1061, 1, 6, 1, 'moda', 359, 'MODELO 3 15SEG', './img/categorias/moda/MODELO-03-15-SEG.jpg', 'https://youtu.be/whenxNweUM8', '2019-12-11 17:54:39', '2019-12-11 17:54:39'),
(1062, 1, 6, 2, 'moda', 899, 'MODELO 3 30SEG', './img/categorias/moda/MODELO-03-30-SEG.jpg', 'https://youtu.be/TUe7hB0Uetw', '2019-12-11 17:54:39', '2019-12-11 17:54:39'),
(1064, 1, 6, 1, 'moda', 359, 'MODELO 4 15SEG', './img/categorias/moda/MODELO-04-15-SEG.jpg', 'https://youtu.be/R5RAoLf6WrI', '2019-12-11 17:54:39', '2019-12-11 17:54:39'),
(1065, 1, 6, 2, 'moda', 899, 'MODELO 4 30SEG', './img/categorias/moda/MODELO-04-30-SEG.jpg', 'https://youtu.be/cxs-CZRDtAY', '2019-12-11 17:54:40', '2019-12-11 17:54:40'),
(1066, 1, 7, 1, 'saúde', 359, 'MODELO 01 15SEG', './img/categorias/saude/MODELO-01-15-SEG.jpg', 'https://youtu.be/4Z9xvM8xDU8', '2019-12-11 18:25:03', '2019-12-11 18:25:03'),
(1067, 1, 7, 2, 'saúde', 899, 'MODELO 01 30SEG', './img/categorias/saude/MODELO-01-30-SEG.jpg', 'https://youtu.be/KkmGbERoVjg', '2019-12-11 18:25:03', '2019-12-11 18:25:03'),
(1068, 1, 7, 1, 'saúde', 359, 'MODELO 02 15SEG', './img/categorias/saude/MODELO-02-15-SEG.jpg', 'https://youtu.be/esN0OlhCMRM', '2019-12-11 18:25:03', '2019-12-11 18:25:03'),
(1069, 1, 7, 2, 'saúde', 899, 'MODELO 02 30SEG', './img/categorias/saude/MODELO-02-30-SEG.jpg', 'https://youtu.be/ib1lYyIpEm8', '2019-12-11 18:25:03', '2019-12-11 18:25:03'),
(1070, 1, 7, 1, 'saúde', 359, 'MODELO 03 15SEG', './img/categorias/saude/MODELO-03-15-SEG.jpg', 'https://youtu.be/sD2AhZH49ho', '2019-12-11 18:25:03', '2019-12-11 18:25:03'),
(1071, 1, 7, 2, 'saúde', 899, 'MODELO 03 30SEG', './img/categorias/saude/MODELO-03-30-SEG.jpg', 'https://youtu.be/zd6jn322XWQ', '2019-12-11 18:25:03', '2019-12-11 18:25:03'),
(1072, 1, 7, 1, 'saúde', 359, 'MODELO 04 15SEG', './img/categorias/saude/MODELO-04-15-SEG.jpg', 'https://youtu.be/-D50dvbXfZQ', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1073, 1, 7, 2, 'saúde', 899, 'MODELO 04 30SEG', './img/categorias/saude/MODELO-04-30-SEG.jpg', 'https://youtu.be/kn4ePeypoIw', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1074, 1, 7, 2, 'saúde', 1299, 'MODELO 04 60SEG', './img/categorias/saude/MODELO-04-60-SEG.jpg', 'https://youtu.be/Rp2ETgG2NdA', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1075, 1, 7, 1, 'saúde', 359, 'MODELO 05 15SEG', './img/categorias/saude/MODELO-05-15-SEG.jpg', 'https://youtu.be/adQrs4MuHnE', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1076, 1, 7, 2, 'saúde', 899, 'MODELO 05 30SEG', './img/categorias/saude/MODELO-05-30-SEG.jpg', 'https://youtu.be/u9GPBErWRk8', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1077, 1, 7, 2, 'saúde', 1299, 'MODELO 05 60SEG', './img/categorias/saude/MODELO-05-60-SEG.jpg', 'https://youtu.be/C6gisxUUs0I', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1078, 1, 7, 1, 'saúde', 359, 'MODELO 06 15SEG', './img/categorias/saude/MODELO-06-15-SEG.jpg', 'https://youtu.be/IUfM0W2GpF0', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1079, 1, 7, 2, 'saúde', 899, 'MODELO 06 30SEG', './img/categorias/saude/MODELO-06-30-SEG.jpg', 'https://youtu.be/uTSypQrBhSQ', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1080, 1, 7, 2, 'saúde', 1299, 'MODELO 06 60SEG', './img/categorias/saude/MODELO-06-60-SEG.jpg', 'https://youtu.be/JdZ832_ZusM', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1081, 1, 7, 1, 'saúde', 359, 'MODELO 07 15SEG', './img/categorias/saude/MODELO-07-15-SEG.jpg', 'https://youtu.be/k5pIFhbGmV8', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1082, 1, 7, 2, 'saúde', 899, 'MODELO 07 30SEG', './img/categorias/saude/MODELO-07-30-SEG.jpg', 'https://youtu.be/uRChCo2BbJM', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1083, 1, 7, 2, 'saúde', 1299, 'MODELO 07 60SEG', './img/categorias/saude/MODELO-07-60-SEG.jpg', 'https://youtu.be/DN7EFtL-rH8', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1084, 1, 7, 1, 'saúde', 359, 'MODELO 08 15SEG', './img/categorias/saude/MODELO-08-15-SEG.jpg', 'https://youtu.be/Pbeyv2pkt0c', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1085, 1, 7, 2, 'saúde', 899, 'MODELO 08 30SEG', './img/categorias/saude/MODELO-08-30-SEG.jpg', 'https://youtu.be/PyjkA1ave10', '2019-12-11 18:25:04', '2019-12-11 18:25:04'),
(1086, 1, 7, 2, 'saúde', 1299, 'MODELO 08 60SEG', './img/categorias/saude/MODELO-08-60-SEG.jpg', 'https://youtu.be/FEccb8OM3Ew', '2019-12-11 18:25:05', '2019-12-11 18:25:05'),
(1087, 1, 8, 1, 'natal', 359, 'MODELO 01 15SEG', './img/categorias/outros/MODELO-01-15-SEG.jpg', 'https://youtu.be/btebgs4lfu4', '2019-12-11 18:41:59', '2019-12-11 18:41:59'),
(1088, 1, 8, 2, 'natal', 899, 'MODELO 01 30SEG', './img/categorias/outros/MODELO-01-30-SEG.jpg', 'https://youtu.be/LbHt6u-wa48', '2019-12-11 18:41:59', '2019-12-11 18:41:59'),
(1089, 1, 8, 2, 'natal', 1299, 'MODELO 01 60SEG', './img/categorias/outros/MODELO-01-60-SEG.jpg', 'https://youtu.be/8KJVK_Rmnng', '2019-12-11 18:41:59', '2019-12-11 18:41:59'),
(1090, 1, 8, 1, 'turismo', 359, 'MODELO 01 15SEG', './img/categorias/outros/TURISMO-MODELO-01-15-SEG.jpg', 'https://youtu.be/FQW3SemyCaE', '2019-12-11 18:41:59', '2019-12-11 18:41:59'),
(1091, 1, 8, 2, 'turismo', 899, 'MODELO 01 30SEG', './img/categorias/outros/TURISMO-MODELO-01-30-SEG.jpg', 'https://youtu.be/ctA4of4KnEk', '2019-12-11 18:41:59', '2019-12-11 18:41:59'),
(1092, 1, 8, 2, 'turismo', 1299, 'MODELO 01 60SEG', './img/categorias/outros/TURISMO-MODELO-01-60-SEG.jpg', 'https://youtu.be/pBTt0Yc_4kc', '2019-12-11 18:41:59', '2019-12-11 18:41:59'),
(1093, 1, 5, 2, 'manutenção', 899, 'MODELO 04 30SEG', './img/categorias/manutencao/MODELO-04-30-SEG.jpg', 'https://youtu.be/ZH5dd4MbIgg', '2019-12-12 13:04:54', '2019-12-12 13:04:54'),
(1094, 1, 8, 2, 'turismo', 899, 'MODELO 02 30SEG', './img/categorias/outros/TURISMO-MODELO-02-30-SEG.jpeg', 'https://youtu.be/GHE2Gr88Zxc', '2019-12-12 14:55:42', '2019-12-12 14:55:42'),
(1095, 1, 8, 1, 'turismo', 359, 'MODELO 02 15SEG', './img/categorias/outros/TURISMO-MODELO-02-15-SEG.jpeg', 'https://youtu.be/fFoFg37LvoQ', '2019-12-12 14:56:30', '2019-12-12 14:56:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `recuperacao_senha`
--

CREATE TABLE `recuperacao_senha` (
  `id` int(11) NOT NULL,
  `id_users` int(11) DEFAULT NULL,
  `ip_server` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id`, `nome`, `observacao`) VALUES
(1, 'ativo', 'para qualquer produto ou users ativado no sistema'),
(2, 'desativado', 'para qualquer produto ou users desativado no sistema');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipos_videos`
--

CREATE TABLE `tipos_videos` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipos_videos`
--

INSERT INTO `tipos_videos` (`id`, `nome`, `observacao`) VALUES
(1, 'vertical', 'para videos do youtube em vertical'),
(2, 'horizontal', 'para videos do youtube em horizontal'),
(0, 'videos 60 seg', 'para videos de 60 segundos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cnpj_cpf` varchar(30) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `ramo` varchar(200) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `logradouro` varchar(200) DEFAULT NULL,
  `bairro` varchar(200) DEFAULT NULL,
  `uf` varchar(10) DEFAULT NULL,
  `cidade` varchar(200) DEFAULT NULL,
  `numero` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `status_id`, `nome`, `cnpj_cpf`, `email`, `senha`, `ramo`, `cep`, `logradouro`, `bairro`, `uf`, `cidade`, `numero`, `created`, `modified`) VALUES
(2, 1, 'lucas', '65.466.546/5465-46', 'teste@shop.com', '$2y$10$lgavvbseceZ50Qb2.Z.lQuzy6afL2QTit53eE.R31rXd7y/Evbvky', 'develop', '09931-070', 'Rua Acácio', 'Campanário', 'SP', 'Diadema', '55', '2020-01-24 11:34:20', '2020-01-24 11:34:20');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `acessorios`
--
ALTER TABLE `acessorios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acessorios` (`tipo_videos_id`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `acessorios`
--
ALTER TABLE `acessorios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `acessorios`
--
ALTER TABLE `acessorios`
  ADD CONSTRAINT `acessorios` FOREIGN KEY (`tipo_videos_id`) REFERENCES `acessorios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
