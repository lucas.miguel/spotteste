<header  class='d-flex cabecalho justify-content-lg-start '>
  <a href="./index.php"><img src="./img/logo/logo.png" alt="" ></a>
  <nav class="d-md-flex">
    <i class="fas fa-bars abrir "></i>
    <i class="fas d-none text-white fechar fa-times position-relative"></i>
    <ul class="menu align-items-center">
      <li><a class="<?php echo $tituloPagina == '' ? 'ativo' : '' ?>" href="./index.php">Home</a></li>
      <li><a class="<?php echo $tituloPagina == 'Categorias' ? 'ativo' : '' ?>" href="./categorias.php">Categorias</a></li>
    </ul>
  </nav>
  
</header>
