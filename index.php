<?php
    include 'banco/conexao.php';
    include 'header/index.php';
    include 'header/menu.php';
    include 'querys/getProdutos.php';
    include 'querys/getCategorias.php';
    
?>
<div class="container">
    <div class="row">
        <?php if($produto == NULL): ?>
            <div class="col-12 msg text-center text-md-left">
                <div class="alert alert-primary text-center" role="alert">
                    nenhum produto cadastrados
                </div>
                <a href="javascript:void(0)"  class="btn btn-outline-primary text-center btn-produto" onClick="modalProduto()"  >Cadastrar Produto</a>
            </div>
                    
        <?php else : ?>
            <div class="col-12 table-responsive border produtosTable" >
                <h5 class="card-title">Produtos cadastrados</h5>
                <table id="tbProdutos" class="table table-ordered table-hover ">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nome do Produto</th>
                            <th>Descrição</th>
                            <th>Categoria</th>
                            <th>Preço</th>
                            <th>criado</th>
                            <th class="text-center">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while($resultado = $results->fetch_object()): ?>
                            <tr>
                                <td><?php echo $resultado->cod_produto ?></td>
                                <td><?php echo $resultado->nomeProduto ?></td>
                                <td><?php echo $resultado->descricaoProduto ?></td>
                                <td><?php echo $resultado->catego ?></td>
                                <td><?php echo $resultado->valor?></td>
                                <td><?php echo $resultado->created ?></td>
                                <td class="d-flex justify-content-around align-items-center">
                                    <a href="javascript:void(0)" role="button" class="btn btn-outline-primary" onClick='editarProduto(<?php echo $resultado->cod_produto ?>)' >Editar</a>
                                    <a href="javascript:void(0)" class="btn btn-outline-danger" onClick="deleteProduto(<?php echo $resultado->cod_produto ?>)" >Excluir</a>
                                </td>
                            </tr>

                        <?php endwhile ?>    
                    </tbody>
                </table>
                <a href="javascript:void(0)"  class="btn btn-outline-primary text-center btn-produto" onClick="modalProduto()"  >Novo produto</a>
            </div>   
        <?php endif ?>     
    </div>
</div>

<?php
//carregando modal
    include 'modal/cadastrarProduto.php';
    include 'modal/modalMsg.php';
    include 'footer/index.php';
?>