

<div class="modal fade" tabindex="-1" role="dialog" id="categoriaModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        
            <form class="form-horizontal" action="#" id="categoriasForm"  method="POST" >
                
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label for="" class="control-label">Nome</label>
                        <div class="input-group">
                            <input type="text" name="nomeCategoria" class="form-control" id="categoriaNome" >
                            <input type="hidden" name="tipoCadastro" id="tipoCadastro">
                            <input type="hidden" name="cod_categoria" id="cod_categoria">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Detalhes</label>
                        <div class="input-group">
                            <input type="text" name="descricaoCategoria" class="form-control" id="descricaoCategoria"  >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Salvar</button>
                    <button type="cancel" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    
                </div>
            </form>
        
        </div>
    
    </div>
</div>