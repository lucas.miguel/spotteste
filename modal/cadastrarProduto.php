

<div class="modal fade" tabindex="-1" role="dialog" id="produtosModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        
            <form class="form-horizontal" id="produtosCrud"  method="POST" >
                
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label for="nomeProduto" class="control-label">Nome</label>
                        <div class="input-group">
                            <input type="text" name="nomeProduto" id="nomeProduto" class="form-control" >
                            <input type="hidden" name="tipoCadastroProdutos" id="tipoCadastroProdutos">
                            <input type="hidden" name="cod_produtos" id="cod_produtos">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="preco" class="control-label">Preço</label>
                        <div class="input-group">
                            <input type="text" name="preco" id="preco" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="detalhe" class="control-label">Detalhes</label>
                        <div class="input-group">
                            <input type="text" name="detalhe" id="detalhe" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="categoria" class="control-label">Categoria</label>
                        <div class="input-group">
                            <?php if($categoria == 0): ?>
                                <small>Categoria nao encontrada</small>
                                <a href="./categorias.php"  class="btn btn-outline-primary" >Cadastrar Categoria</a>
                            
                            <?php else: ?>
                                <select name="categoria" id="categoria">
                                    <?php while($resultadoCategoria = $resultsCategorias->fetch_object()):?>
                                        <option value="<?php echo $resultadoCategoria->cod_categoria ?>"><?php echo $resultadoCategoria->nome ?></option>
                                    <?php endwhile ?>
                                </select>
                            <?php endif ?>        
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Salvar</button>
                    <button type="cancel" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    
                </div>
            </form>
        
        </div>
    
    </div>
</div>